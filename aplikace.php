<?php get_header(); ?>
<?php /* Template Name: aplikace */ ?>

<section class="section1 header_background_foto_aplikace">
        <div class="container" >
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <div class="wrapper_header_background_foto">
                <div class="wrapper_intro_foto">
                  <div class="item_intro_foto1">
                    <img class="item_intro_foto1_img" src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                  </div>
                  <div class="item_intro_foto2">
                    <a  href="https://apps.apple.com/cz/app/za%C5%BEijkraj-cz/id1520204546" target="_blank"><img class="item_intro_foto2_img" src="<?php echo get_template_directory_uri(); ?>/img/appstore.png" alt=""></a>
                    <a  href="https://play.google.com/store/apps/details?id=com.pressprojekt.zazijkraj&hl=cs&gl=US&pli=1" target="_blank"><img class="item_intro_foto2_img" src="<?php echo get_template_directory_uri(); ?>/img/google_play.png" alt=""></a>
                  </div>
                </div>
                <div class="header_foreground_text header_foreground_text_aplikace">
                  <div class="wrapper_green_line_h1">
                    <div class="header_green_line"></div>
                    <h1><?php the_field('aplikace_cf7', get_the_ID()); ?></h1>
                  </div>
                  <a href="#swiper" ><button class="header_button button_hover"><?php the_field('aplikace_cf8', get_the_ID()); ?></button></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section2 section2_swipe_aplikace">
        <div class="container-fluid" >
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 header_slider">

              <div class="controls">
                <div class="prev"> <img src="<?php echo get_template_directory_uri(); ?>/img/prev.svg" alt=""></div>
                <div class="next"> <img src="<?php echo get_template_directory_uri(); ?>/img/next.svg" alt=""></div>
              </div>

            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 aplikace">
              <div class="swiper aplikace_slider" id="swiper">
                <div class="swiper-wrapper">

                  <div class="swiper-slide">
                    <div class="swiper-slide_uvod_foto"><img src="<?php echo get_template_directory_uri(); ?>/img/swiper-slide_uvod_foto.png" alt=""></div>
                  </div>

                  <div class="swiper-slide">
                    <div class="slide_celek2">
                      <div class="slide_celek2_nadpis">
                        <h1>Tvořeno ve <br>spolupráci s <br><span>profíky</span></h1>
                      </div>
                      <div class="slide_celek2_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                      </div>
                      <div class="slide2_text">
                        <div class="slide2_text1">
                          <h2>OBSAH OD LEGEND<br> ČESKÝCH PRŮVODCŮ</h2>
                          <p>Aplikace nabízí kvalitní<br> texty od autorů knížek z <br>nakladatelství <br>Soukup & David</p>
                        </div>
                        <div class="slide2_text2">
                          <h2>CESTOVATELKÝ <br>DENÍK, KTERÝ SI <br>PÍŠETE VY</h2>
                          <p>Uchovejte si vaše <br>cestovatelké zážitky ve <br>svém osobním <br>cestovatelském deníku</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="slide_celek3">
                      <div class="slide3_text">
                        <div class="slide3_text_nadpis">
                          <h1>Appka pro <br>všechny milovníky <br>cestování</h1>
                        </div>
                        <div class="slide3_text_podnadpis">
                          <h2> STOVKY VÝLETŮ PŘÍMO VE VAŠÍ<br> KAPSE K DISPOZICI KDEKOLIV</h2>
                          <p>Provedeme vás napříč celou Českou republikou. <br> Stovky výletú, 
                            které nesmí chybět v mobilu <br>žádného modernímu cestovateli.</p>
                        </div>
                      </div>
                      <div class="slide_celek3_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                      </div>
                    </div>
                  </div>

                  <div class="swiper-slide">
                    <div class="slide_celek4">
                      <div class="slide_celek4_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                      </div>
                      <div class="slide4_text">
                        <div class="slide4_text_nadpis">
                          <h1>Objevujte, <br>poznávejte,<br>bavte se.</h1>
                        </div>
                        <div class="slide4_textp">
                          <p>Na své si příjde fanoušek historických památek i <br>
                            panenské přírody. Poznat můžete krásná města <br>
                            nebo jen relaxovat v tajuplných lesich </p>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div class="swiper-pagination"></div>
            </div>
        </div>
      </section>
      <section class="section2_small_device">
        <div class="container section2_small_device">
          <div class="row section2_small_device">
            <div class="col-md-12 col-sm-12 col-xs-12">
              
                <div class="sm1">
                  <div class="sm1_nadpis">
                    <h1>Tvořeno ve spolupráci s <span>profíky</span></h1>
                  </div>
                  <div class="slide2_text1">
                    <h2>OBSAH OD LEGEND<br> ČESKÝCH PRŮVODCŮ</h2>
                    <p>Aplikace nabízí kvalitní texty od autorů knížek z nakladatelství Soukup & David</p>
                  </div>
                  <div class="sm1_img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                  </div>
                  <div class="sm1_text2">
                    <h2>CESTOVATELKÝ DENÍK, KTERÝ SI PÍŠETE VY</h2>
                    <p>Uchovejte si vaše cestovatelké zážitky ve svém osobním cestovatelském deníku</p>
                  </div>
                </div>

                <div class="sm2">
                  <div class="sm2_text_nadpis">
                    <h1>Appka pro všechny milovníky cestování</h1>
                  </div>
                  <div class="sm2_img">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                  </div>
                  <div class="sm2_text_podnadpis">
                    <h2> STOVKY VÝLETŮ PŘÍMO VE VAŠÍ KAPSE K DISPOZICI KDEKOLIV</h2>
                    <p>Provedeme vás napříč celou Českou republikou. Stovky výletú, 
                      které nesmí chybět v mobilu žádného modernímu cestovateli.</p>
                  </div>
                </div>

                <div class="sm3">
                    <div class="sm3_text_nadpis">
                      <h1>Objevujte, poznávejte, bavte se.</h1>
                    </div>
                    <div class="sm3_img">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                    </div>
                    <div class="sm3_textp">
                      <p>Na své si příjde fanoušek historických památek i 
                        panenské přírody. Poznat můžete krásná města 
                        nebo jen relaxovat v tajuplných lesich </p>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </section>

      <section class="section2">
        <div class="container">
            <div class="row">
                <div class=" col-md-12 col-sm-12 col-xs-12 button_after_swiper">
                  <a  href="https://apps.apple.com/cz/app/za%C5%BEijkraj-cz/id1520204546" target="_blank"><img class="" src="<?php echo get_template_directory_uri(); ?>/img/appstore.png" alt=""></a>
                  <a  href="https://play.google.com/store/apps/details?id=com.pressprojekt.zazijkraj&hl=cs&gl=US&pli=1" target="_blank"><img class="" src="<?php echo get_template_directory_uri(); ?>/img/google_play.png" alt=""></a>
                  <a  href="Aplikace_VIP.html"><img class="" src="<?php echo get_template_directory_uri(); ?>/img/VIP.svg" alt=""></a>
                </div>
            </div>
        </div>
      </section>

<?php get_footer(); ?>