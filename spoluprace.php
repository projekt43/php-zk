<?php get_header(); ?>
<?php /* Template Name: spolupráce */ ?>


<section class="section1 header_background_foto_spoluprace">
    <div class="container" >
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 ">
          <div class="wrapper_header_background_foto">
            <div class="header_foreground_text header_foreground_text_spoluprace">
              <div class="wrapper_green_line_h1">
                <div class="header_green_line"></div>
                <h1>Spolupráce, <br>která dává <br><span>smysl.</span></h1>
              </div>
              <a href="#jste_hotel" ><button class="header_button button_hover">Zjistit více</button></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
    
    <section>
      <div class="section2 max_vyska">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="jste_hotel" id="jste_hotel">
                    <h1 class="h1_newText">Jste <span id="newText" ></span><span id="newText_small_device"></span></h1>
                    <h1 class="h1_newText_small_device">Jste <span>hotel</span>, <span>restaurace</span>, <span>rozhledna</span>, nebo zajímavá
                       <span>turistická lokalita</span>?</h1>
                </div>
                <div class="wrapper_jste">

                    <div class="item_jste_text1">
                        <h2>Inzerujte v naší aplikaci</h2>
                        <p class="item_jste_text1_p">Reklama na váš podnik, restauraci, hotel, 
                            nebo třeba rozhlednu přímo u výletu v naší appce. Uvidí ji tak každý,
                             kdo bude chtít na dané místo vyrazit.</p>
                    </div>

                    <div class="item_jste_img">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                    </div>

                    <div class="item_jste_text2">
                        <h2>Přidáme vás do výletů</h2>
                        <p >Pokud spravujete turisticky významné místo, rádi vás přidáme na mapu výletů.
                             Díky tomu vás uvidí a bude moci navštívit ještě více cestovatelů.</p>
                    </div>
                </div>
                <div class="Jste_footer">
                    <p>Více o naší appce se dozvíte  <a class="a_decoration_none" href="Aplikace.html">zde</a></p>
                    <a href=""><button>Napíšeme o vás ▼</button></a>
                </div>
            </div>

         </div>
        </div>
      </div>
    </section>
    <section>
      <div class="section3 max_vyska">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 napiseme_o_vas">
              <h1 class="h1_newText">Napíšeme o vás na <span id="newText2"></span></h1>
              <h1 class="h1_newText_small_device">Napíšeme o vás na <span>blogu</span>, <span>Facebooku</span>,
                nebo <span>Instagramu</span></h1>



            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 wrapper_napiseme_o_vas">
              <p>Dokážeme váš podnik zviditelnit, ať už u nás na blogu, nebo na sociálních sítích.<br>
                 Napíšeme o vás článek, nebo přidáme vaši reklamu. Dostane se tak ke stovkám turistů po 
                 České republice.</p>
              <a href="Blog.html"><img src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt=""></a>
              <a href=""><button>Marketing</button></a>
            </div>
          </div>
        </div>
      </div>
      <div class="section4">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 wrapper_marketing">
              <div> <h1><?php the_field('spoluprace_cf6', get_the_ID()); ?></h1></div>

              <div class="wrapper_marketing_icon">
                        <?php if( have_rows('spoluprace_cf9', get_the_ID()) ): ?>	
                  <?php while( have_rows('spoluprace_cf9', get_the_ID()) ): the_row(); ?>

                          <div class="item_marketing_icon">
                            <img src="<?php echo wp_get_attachment_image_url(get_sub_field('spoluprace_cf9_1'), 'large'); ?>" alt="">
                            <div>
                              <h4><?php the_sub_field('spoluprace_cf9_2'); ?> </h4>
                              <p><?php the_sub_field('spoluprace_cf9_3'); ?> </p>
                            </div>
                          </div>

                    <?php endwhile; ?>
                  <?php endif; ?>
              </div>
              <div class="button_kontaktujte_nas"><a href="index.html#Kontakt"><button><?php the_field('spoluprace_cf7', get_the_ID()); ?></button></a></div>
              
            </div>
          </div>
        </div>
      </div>
    </section>


<?php get_footer(); ?>