<?php get_header(); ?>
<?php /* Template Name: blog */ ?>

<section class="section1 header_background_foto_blog">
        <div class="container" >
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <div class="wrapper_header_background_foto">
                <div class="header_foreground_text header_foreground_text_blog">
                  <div class="wrapper_green_line_h1">
                    <div class="header_green_line"></div>
                    <h1>Inspirace na <br>výlety jedině <br>u nás na<br><span>blogu.</span></h1>
                  </div>
                  <a href="#jste_hotel" ><button class="header_button button_hover">Zjistit více</button></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div class="section3">
        <div class="container-fluid">
          <div class="row">
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 ">
                <a href="" class="blog_card_item" style="background-image: url(./img/blog_card.webp)" >
                  <div class="blog_card_item_top">
                    <p class="publish_date">22. 9. 2022</p>
                    <p class="time_read">3min</p>
                  </div>
                  <div class="blog_card_item_bottom">
                    <p class="blog_card_title"> 5 deskovek na víkend, když vás počasí nepustí ven</p>
                  </div>
                </a>
              </div>
         </div>
        </div>
      </div>

      <div class="section2">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                
                <div class="wrapper_clanek">

                    <div class="item_clanek_background">
                        <div>
                            <p></p>

                        </div>
                        <div><a href=""><p></p></a></div>
                    </div>

                </div>

            </div>

         </div>
        </div>
      </div>

<?php get_footer(); ?>