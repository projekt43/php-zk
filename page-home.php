<?php get_header(); ?>
<?php /* Template Name: Úvodní stránka */ ?>


    <section class="section1 header_background_foto_index" style="background-image: url(<?php echo wp_get_attachment_image_url(get_field('home_cf1', get_the_ID()), 'full'); ?>)">
        <div class="container" >
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <div class="header_foreground_text header_foreground_text_index">
                  <div class="wrapper_green_line_h1">
                    <div class="header_green_line"></div>

                    <?php if(get_field('home_cf2', get_the_ID())): ?>
                      <h1><?php the_field('home_cf2', get_the_ID()); ?></h1>
                    <?php endif;?>

                  </div>
                  <a href="<?php the_field('home_cf4', get_the_ID()); ?>" target="_blank" class="button"><button class="header_button button_hover"><?php the_field('home_cf3', get_the_ID()); ?></button></a>
                </div>
              </div>
            </div>
        </div>
    </section>

      <section class="section2">
        <div class="container" >
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                 <div class="kdo_jsme">
                    <h2><?php the_field('home_cf5', get_the_ID()); ?></h1>
                    <p class="kdo_jsme_paragraph_one"> <?php the_field('home_cf6', get_the_ID()); ?></p>
                </div>
            </div>
        </div>
      </section>

      <section class="section3">
        <div class="container" >
          <div class="row odsazeni_od_stran">
            <div class=" col-md-12 col-sm-12 col-xs-12 folder_wrapper">



              <?php if( have_rows('home_cf7', get_the_ID()) ): ?>
    <?php while( have_rows('home_cf7', get_the_ID()) ): the_row(); ?>
        
        <div class="item_wrapper">
                <div class="item_wrapper_background">
                  <div class="item_wrapper_foto">
                    <img src="<?php echo wp_get_attachment_image_url(get_sub_field('home_cf7_1'), 'large'); ?>" alt="">
                  </div>
                  <div class="item_wrapper_content">
                    <h2><?php the_sub_field('home_cf7_2'); ?></h2>
                    <p><?php the_sub_field('home_cf7_3'); ?></p>
                    <a class="a_decoration_none" href="<?php the_sub_field('home_cf7_4'); ?>"><h3><?php the_sub_field('home_cf7_5'); ?></h3></a>
                  </div>
                </div>
              </div>
    <?php endwhile; ?>
<?php endif; ?>

       </div>
      </section>

      <section class="section4">
        <div class="container" >
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="Kontakt" id="Kontakt">
                <h2><?php the_field('home_cf8', get_the_ID()); ?></h1>
              </div>

              <div class="wrapper_kontakt">

              <?php if( have_rows('home_cf9', get_the_ID()) ): ?>
    <?php while( have_rows('home_cf9', get_the_ID()) ): the_row(); ?>

                <div class="item_konkt">
                  <div class="obchodni_spoluprace">
                    <h3><?php the_sub_field('home_cf9_1'); ?></h2>
                    <p><?php if(get_field('home_cf9_5', get_the_ID())): ?><img src="<?php echo wp_get_attachment_image_url(get_sub_field('home_cf9_5'), 'large'); ?>" alt=""><?php endif;?><?php if(get_field('home_cf9_2', get_the_ID())): ?><?php the_sub_field('home_cf9_2'); ?><?php endif;?><br>
                      <?php if(get_field('home_cf9_6', get_the_ID())): ?><img src="<?php echo wp_get_attachment_image_url(get_sub_field('home_cf9_6'), 'large'); ?>" alt=""><?php endif;?><?php the_field('home_cf9_3', get_the_ID()); ?> <br>
                        <?php if(get_field('home_cf9_7', get_the_ID())): ?><img src="<?php echo wp_get_attachment_image_url(get_sub_field('home_cf9_7'), 'large'); ?>" alt=""><?php endif;?><?php the_field('home_cf9_4', get_the_ID()); ?></p>
                  </div>
                </div>

    <?php endwhile; ?>
<?php endif; ?>

                


                
                <div class="PressProjekt">
                  <img src="<?php echo wp_get_attachment_image_url(get_field('home_cf10', get_the_ID()), 'large'); ?>" alt="">
                </div>
              </div>

              <div class="sidlo">
                <p><?php the_field('home_cf11', get_the_ID()); ?></p>
              </div>
              <div class="najdete_nas">
                <h2><?php the_field('home_cf12', get_the_ID()); ?></h2>
                <p><?php the_field('home_cf13', get_the_ID()); ?></p>
              </div>
              <div class="wrapper_soc">

              <?php if( have_rows('home_cf14', get_the_ID()) ): ?>				
    <?php while( have_rows('home_cf14', get_the_ID()) ): the_row(); ?>

    <div class="item_soc col-md-3 col-sm-3 col-xs-6 item_soc"><a class="a_decoration_none" href="<?php the_field('home_cf14_3', get_the_ID()); ?>"><img src="<?php echo wp_get_attachment_image_url(get_sub_field('home_cf14_1'), 'large'); ?>" alt=""><p><?php the_sub_field('home_cf14_2'); ?></p></a></div>

    <?php endwhile; ?>
<?php endif; ?>
                


              </div>
            </div>
          </div>
        </div>
      </section>

      <?php get_footer(); ?>