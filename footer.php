<footer class="footer">
        <div class="container">
            <div class="row">
                <div class=" col-md-12 col-sm-12 col-xs-12">
                    <p><?php the_field('home_cf15', get_the_ID()); ?></p>
                </div>
            </div>
        </div>
      </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/my.js"></script>

  </body>
</html>