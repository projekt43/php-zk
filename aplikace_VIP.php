<?php get_header(); ?>
<?php /* Template Name: aplikace_VIP */ ?>

<section class="section1 header_background_foto_aplikace">
        <div class="container" >
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
              <div class="wrapper_header_background_foto">
                <div class="wrapper_intro_foto">
                  <div class="item_intro_foto1">
                    <img class="item_intro_foto1_img" src="<?php echo get_template_directory_uri(); ?>/img/telefon.png" alt="">
                  </div>
                  <div class="item_intro_foto2">
                    <a  href="https://apps.apple.com/cz/app/za%C5%BEijkraj-cz/id1520204546" target="_blank"><img class="item_intro_foto2_img" src="<?php echo get_template_directory_uri(); ?>/img/appstore.png" alt=""></a>
                    <a  href="https://play.google.com/store/apps/details?id=com.pressprojekt.zazijkraj&hl=cs&gl=US&pli=1" target="_blank"><img class="item_intro_foto2_img" src="<?php echo get_template_directory_uri(); ?>/img/google_play.png" alt=""></a>
                  </div>
                </div>
                <div class="header_foreground_text header_foreground_text_aplikace">
                  <div class="wrapper_green_line_h1">
                    <div class="header_green_line"></div>
                    <h1>Maximum <br>z naší app<br> s <span>VIP</span></h1>
                  </div>
                  <a href="#vip" ><button class="header_button button_hover">Více o VIP</button></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div class="section2">
        <div class="container">
            <div class="row">
                <div class=" col-md-12 col-sm-12 col-xs-12 vip" id="vip">

                  <h2>Staňte se členem a užívejte si cestování naplno!</h2>

                  <button>VIP výhody</button>

                  <div class="wrapper_vip_vyhody">
                    <div class="item_vip_vyhody">
                      <div class="item_vip_frame">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Frame_stan.svg" alt="">
                      </div>
                      <p>Veškeré podrobnosti
                        o místech a doporučení profesionálního cestovatele</p>
                    </div>
                    <div class="item_vip_vyhody">
                      <div class="item_vip_frame">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Frame_fotak.svg" alt="">
                      </div>
                      <p>Možnost nahrát více fotografií do svého deníčku</p>
                    </div>
                    <div class="item_vip_vyhody">
                      <div class="item_vip_frame">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Frame_kostky.svg" alt="">
                      </div>
                      <p>Hry na cestu pro malé i velké</p>
                    </div>
                    <div class="item_vip_vyhody">
                      <div class="item_vip_frame">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Frame_telefon.svg" alt="">
                      </div>
                      <p>Poslechněte si informace o výletech jako audioverzi - u vybraných míst</p>
                    </div>
                    <div class="item_vip_vyhody">
                      <div class="item_vip_frame">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Frame_sleva.svg" alt="">
                      </div>
                      <p>Slevové kupóny
                        k našim partnerům</p>
                    </div>
                    <div class="item_vip_vyhody">
                      <div class="item_vip_frame"> 
                        <img src="<?php echo get_template_directory_uri(); ?>/img/frame_mapa.png" alt="">
                      </div>
                      <p>Možnost měnit podklady mapy na leteckou nebo turistickou</p>
                    </div>
                  </div>

                  <button>Proč si VIP účet pořídit?</button>

                  <div class="wrapper_vip_vyhody">
                    <div class="item_vip_vyhody">
                      <div class="item_vip_frame">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Frame_zazitky.svg" alt="">
                      </div>
                      <p>Veškeré podrobnosti
                        o místech a doporučení profesionálního cestovatele</p>
                    </div>
                    <div class="item_vip_vyhody">
                     <div class="item_vip_frame">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Frame_priroda.svg" alt="">
                     </div>
                      <p>Možnost nahrát více fotografií do svého deníčku</p>
                    </div>
                    <div class="item_vip_vyhody">
                     <div class="item_vip_frame">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/Frame_strom.svg" alt="">
                     </div>
                      <p>Hry na cestu pro malé i velké</p>
                    </div>
                  </div>

                  <p class="vip_cena">Za 29,- Kč / měsíc, 299,- za rok nebo 599,- Kč jednorázově</p>

                  <div class="odkazy_na_stazeni">
                    <a  href="https://apps.apple.com/cz/app/za%C5%BEijkraj-cz/id1520204546" target="_blank"><img class="" src="<?php echo get_template_directory_uri(); ?>/img/appstore.png" alt=""></a>
                    <a  href="https://play.google.com/store/apps/details?id=com.pressprojekt.zazijkraj&hl=cs&gl=US&pli=1" target="_blank"><img class="" src="<?php echo get_template_directory_uri(); ?>/img/google_play.png" alt=""></a>
                  </div>

                  </div>
            </div>
        </div>
      </div>

<?php get_footer(); ?>