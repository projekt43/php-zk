<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/media.css">

  </head>
  <body>
    <header class="header">
       <div class="container-fluid">
         <div class="row">
           <div class="col-md-12">
             <div class="header_flex">
               <div class="wrapper_logo_img">
                 <a href="index.html"><img class="logo_img" src="<?php echo get_template_directory_uri(); ?>/img/Logo_zk.png" alt="Zažijkraj logo"></a>
               </div>
               <div>
                  <ul class="navmenu">
                    <li><a href="index.html"><div class="header_menu_text"><span>O nás</span></div></a></li>
                    <li><a href="Aplikace.html"><div class="header_menu_text"><span>Aplikace</span></div></a></li>
                    <li><a href="Spoluprace.html"><div class="header_menu_text"><span>Spolupráce</span></div></a></li>
                    <li><a href="blog.html"><div class="header_menu_text"><span>Blog</span></div></a></li>
                    <li><a href="index.html#Kontakt"><div class="header_menu_text"><span>Kontakt</span></div></a></li>
                    <li><a href="https://eshop.zazijkraj.cz/" target="_blank"><div class="header_menu_text"><span>Eshop</span></div></a></li>
                 </ul>
                </div>
                <div class="open_menu">
                  <img src="<?php echo get_template_directory_uri(); ?>/img/menu.svg"  alt="menu open button" >
                </div>
              </div>
            </div>
           </div>
         </div>
      </header>   
